#!/usr/bin/env python

"""
MODULE:    v.net.tripplanner

AUTHOR(S): Vaclav Petras <wenzeslaus@gmail.com>

PURPOSE:   Plans trips in a network

COPYRIGHT: (C) 2019-2020 Vaclav Petras, and the GRASS Development Team

           This program is free software under the GNU General Public
           License (>=v2). Read the file COPYING that comes with GRASS
           for details.
"""

#%module
#% description: Splits network into trips
#% keyword: vector
#% keyword: network
#% keyword: salesman
#%end
#%option G_OPT_V_INPUT
#% description: Vector network
#% required: yes
#%end
#%option G_OPT_V_CAT
#% key: from_cat
#% label: Home
#% description: Category of the starting and ending location of each trip
#%end
#%option G_OPT_V_CATS
#% key: to_cats
#% label: Destinations
#% description: Categories of all the places to visit
#%end
#%option
#% key: output_basename
#% type: string
#% label: Trips
#% description: Vector maps of the trips
#% required: yes
#%end
#%option
#% key: max_time
#% type: double
#% description: Maximum time per one trip
#% required: yes
#% multiple: yes
#%end
#%option
#% key: place_time
#% type: double
#% description: Time spent at each place
#% required: yes
#%end
#%option
#% key: speed
#% type: double
#% label: Travel speed through the network
#% description: For example, average driving speed
#% required: yes
#%end
#%option
#% key: min_places
#% type: integer
#% label: Minimum of places to visit
#% description: Minimum number of places to visit during one trip
#% required: no
#%end
#%option G_OPT_V_INPUT
#% key: attribute_map
#% label: Places with their attributes
#% description: Attributes of these points will be copied to each trip
#% required: no
#% guidependency: multiplace_column,name_column
#%end
#%option G_OPT_DB_COLUMN
#% key: multiplace_column
#% label: Column with identification of multiplaces
#% description: Identification of destinations which should be grouped together on one trip
#% required: no
#%end
#%option G_OPT_DB_COLUMN
#% key: name_column
#% label: Column with name of a place
#% description: Name is used in error messages besides category
#% required: no
#%end
#%rules
#% requires: multiplace_column,attribute_map
#% requires: name_column,attribute_map
#%end

from __future__ import print_function
from __future__ import division

from itertools import chain

import grass.script as gs


def less_than_any(value, items):
    """Is the value less then any of the values in list items?"""
    return any([value <= item for item in items])


def any_diff_smaller_than(value, items, difference):
    """Is any of the differences between value and items smaller than difference?"""
    assert difference > 0
    return any([abs(value - item) < difference for item in items])


def to_cats_parameter(cats, collapse=True):
    """Converts list of categories into user-readable string

    Removes any duplicates and sorts the list.
    Note that sorting may not be desired for some
    use cases, although it provides usually better or same result.

    Collapsing takes consecutive categoris and outputs them as range.
    One or more ranges can appear in the output combined with individual
    non-consecutive categories.

    Categories are assumed to be positive integers.

    >>> to_cats_parameter([1, 2, 3])
    1,2,3
    >>> to_cats_parameter([3, 6])
    3,6
    >>> to_cats_parameter([2, 4, 5, 6, 10, 11])
    2,4-6,10,11
    >>> to_cats_parameter([2, 4, 9, 15])
    2,4,9,15
    >>> to_cats_parameter([7, 16, 48, 3, 17, 47, 2, 9, 4, 2, 9, 4])
    2-4,7,9,16,17,47,48
    >>> to_cats_parameter([5, 14, 9, 4, 9, 1, 4])
    1,4,5,9,14
    """
    assert cats, "The cats parameter must not be empty"

    def append(result, lower_bound, upper_bound):
        """Append another category interval to the list

        If lower and upper bounds are the same, it adds one number.
        If lower and upper bounds are two consecutive numbers, it adds both.
        Otherwise, an interval is added.
        All numbers and intervals are added as strings.
        """
        assert lower_bound <= upper_bound
        if lower_bound == upper_bound:
            result.append(str(lower_bound))
        elif lower_bound + 1 == upper_bound:
            result.append(str(lower_bound))
            result.append(str(upper_bound))
        else:
            result.append("{}-{}".format(lower_bound, upper_bound))

    cats = sorted(set(cats))
    if not collapse:
        return ",".join([str(i) for i in cats])
    # go over cats and collapse list to range if cats are consecutive numbers
    result = []
    lower_bound = cats[0]
    upper_bound = None
    for cat in cats:
        if (upper_bound is not None) and (upper_bound + 1 != cat):
            append(result, lower_bound, upper_bound)
            lower_bound = cat
        upper_bound = cat
    append(result, lower_bound, upper_bound)
    return ",".join(result)


def flatten_list(list_of_lists):
    """"Flatten one level of nesting"""
    return chain.from_iterable(list_of_lists)


def unpack_cats(map_name, layer, cats):
    """Get full list of categories from a map.

    This assumes that the vector map does not have two cats per feature in
    one layer otherwise the parsing of v.category output will fail.
    """
    cats = gs.read_command(
        "v.category", input=map_name, layer=layer, cat=cats, option="print"
    )
    return [int(cat) for cat in cats.strip().splitlines()]


def column_for_category(map_name, layer, column, category):
    """Return value of a specified column for given category"""
    # assuming key column is 'cat'
    result = gs.vector_db_select(
        map=map_name, layer=layer, where="cat = {}".format(category), columns=column
    )
    return result["values"][category][0]


def query_vector(map_name, layer, query_map, query_layer, columns=None):
    """Add all column values to a vector from another vector"""
    # We use super quiet to avoid warning about column cat being
    # overwritten which is not the case with v.db.addtable.
    gs.run_command("v.db.addtable", map=map_name, layer=layer, quiet=True)
    query_columns = gs.vector_columns(query_map, layer=query_layer, getDict=True)
    if not columns:
        columns = query_columns.keys()
    for column in columns:
        # TODO: this assumes that key column is always called cat
        # we ignore cat even if user requested it
        if column == "cat":
            continue
        # number of columns is generally low, so we don't mind searching
        # in them even in case when we are using all columns
        # and we prefer that over iterating over them to get the order
        # requested by the user in the parameter
        column_type = query_columns[column]["type"]
        column_def = "{} {}".format(column, column_type)
        gs.run_command(
            "v.db.addcolumn", map=map_name, layer=layer, columns=column_def, quiet=True
        )
        gs.run_command(
            "v.what.vect",
            map=map_name,
            layer=layer,
            column=column,
            query_map=query_map,
            query_layer=query_layer,
            query_column=column,
            quiet=True,
        )


def examine_place_and_fatal_error(
    category,
    trip_places,
    remaining_places,
    current_map=None,
    current_layer=None,
    master_map=None,
    master_layer=None,
    multiplace_column=None,
    name_column=None,
):
    """Ends with fatal error for a place in trip not originally requested

    The function issues a detailed fatal error (gs.fatal) when a place not
    requested in the given step ends up being in a trip anyway.
    This is ususally related to multiple points with same coordinates.
    """
    if name_column:
        name = column_for_category(
            current_map, layer=current_layer, column=name_column, category=category
        )
    else:
        name = None

    place_identifier = None
    all_related_categories = None
    if multiplace_column:
        place_identifier = column_for_category(
            current_map,
            layer=current_layer,
            column=multiplace_column,
            category=category,
        )
        if place_identifier:
            # TODO: the value is now assumed to be string
            query_result = gs.vector_db_select(
                map=current_map,
                layer=current_layer,
                where="{column} = '{value}'".format(
                    column=multiplace_column, value=place_identifier
                ),
                columns="cat",
            )
            all_related_categories = query_result["values"].keys()
    master_place_identifier = None
    master_all_related_categories = None
    master_and_current_same = False
    if master_map:
        master_place_identifier = column_for_category(
            master_map, layer=master_layer, column=multiplace_column, category=category
        )
        if master_place_identifier:
            # TODO: the value is now assumed to be string
            query_result = gs.vector_db_select(
                map=master_map,
                layer=master_layer,
                where="{column} = '{value}'".format(
                    column=multiplace_column, value=master_place_identifier
                ),
                columns="cat",
            )
            master_all_related_categories = query_result["values"].keys()
        if set(all_related_categories) == set(master_all_related_categories):
            master_and_current_same = True

    message = _(
        "Trip contains a point which was not requested in the"
        " given step. This suggests that there are duplicate"
        " points in the dataset, i.e., two or more points with"
        " same coordinates. These will need to be cleaned up"
        " or identified using multiplace_column option."
        " Details follow.\n"
    )
    if name:
        message += _("Place: {category} - {name}\n").format(**locals())
    else:
        message += _("Place: {category}\n").format(**locals())
    if place_identifier:
        message += _("Multiplace identifier: {place_identifier}\n").format(**locals())
    if master_and_current_same:
        if len(all_related_categories) == 1:
            message += _(
                "This place is not identified as a multiplace in the current step\n"
            )
        elif all_related_categories:
            message += _(
                "All places in the multiplace in the current step: {cats}\n"
            ).format(cats=to_cats_parameter(all_related_categories, collapse=False))
    else:
        if len(master_all_related_categories) >= 1:
            message += _(
                "This place was not identified as a multiplace in"
                " the current step, but it is a multiplace\n"
            )
            message += _("All places in the multiplace: {cats}\n").format(
                cats=to_cats_parameter(master_all_related_categories, collapse=False)
            )
    message += _("Places on the trip: {cats}\n").format(
        cats=to_cats_parameter(trip_places)
    )
    message += _("Places requested: {cats}").format(
        cats=to_cats_parameter(remaining_places)
    )
    gs.fatal(message)


# TODO: report as CSV with descriptive columns
# (parseable and/versus human-readable report-like format)
def report(trips, times, never_resolved, user_min_places, max_time_per_trip):
    """Print final report from the optimization

    :trips: list of trips (list of lists)
    :times: list of times for each trip
            (pairs of actual time and maximum allowed time for one trip)
    :never_resolved: lits of places never placed on any trip
    :user_min_places: mimimum number of places as specified by the user
    :max_time_per_trip: maximum time for one trip as specified by the user
    """
    num_places = 0
    total_time = 0
    total_max_time = 0
    num_trips_with_low_num_places = 0
    num_places_missing_in_trips = 0
    for i, trip in enumerate(trips):
        print(
            "Trip {order}"
            " ({num} places, time: {trip_time:.1f} ({max_time:.0f})): {cats}".format(
                order=i + 1,
                num=len(trip),
                cats=to_cats_parameter(trip),
                trip_time=times[i][0],
                max_time=times[i][1],
            )
        )
        num_places += len(trip)
        time = times[i][0]
        max_time = times[i][1]
        # TODO: here we should account for lowering num of places,
        # so the trip can potentially fit into a shorter time
        # so we should not compare it with the allowed max
        # alternatively, we could drop the max trip time value during
        # the calculation either for the current trip or also for all
        # the following trips
        total_time += time
        total_max_time += max_time
        if user_min_places and len(trip) < user_min_places:
            num_trips_with_low_num_places += 1
            num_places_missing_in_trips += user_min_places - len(trip)
    time_efficiency = 100.0 * total_time / total_max_time
    num_trips = len(trips)
    # TODO: rename this as places per trip
    trips_places_ratio = num_places / num_trips
    if num_trips_with_low_num_places:
        # TODO: special case for num_trips_with_low_num_places == 1
        # TODO: "all good" message for num_trips_with_low_num_places == 0
        average_missing = (
            float(num_places_missing_in_trips) / num_trips_with_low_num_places
        )
        print(
            "{trips} trips have less places then requested"
            " (average missing: {avg:.0f}, total missing: {total})".format(
                trips=num_trips_with_low_num_places,
                avg=average_missing,
                total=num_places_missing_in_trips,
            )
        )
    print(
        "Using {time:.2f} hours from {max_time:.2f} allocated"
        " ({efficiency:.1f}% allocated time efficiency)".format(
            time=total_time, max_time=total_max_time, efficiency=time_efficiency
        )
    )
    print(
        "Covering {num} places in {time:.2f} hours"
        " ({ratio:.1f} places per hour (places-time ratio))".format(
            num=num_places, time=total_time, ratio=num_places / total_time
        )
    )
    print(
        "Covering {num} places in {trips} trips"
        " ({ratio:.1f} places per trip)".format(
            num=num_places, trips=num_trips, ratio=trips_places_ratio
        )
    )
    # TODO: the never_resolved contains also home_cat
    if never_resolved:
        print(
            "Due to the time constrains ({time}), {num} places"
            " remain unresolved: {cats}".format(
                num=len(never_resolved),
                cats=to_cats_parameter(never_resolved),
                time=max_time_per_trip,
            )
        )


def get_collapsing_message(place_identifier, all_related_categories):
    """Return message about collapsing points into a multipoint"""
    message = _("Collapsing as multipoint based on {id}: {cats}").format(
        id=place_identifier,
        cats=to_cats_parameter(all_related_categories, collapse=False),
    )
    return message


def find_trip(
    salesman,
    salesman_filename,
    to_home_filename,
    home_category,
    max_time_per_trip,
    time_per_place,
    dist_to_time,
    multiplace_column=None,
):
    """Find one trip in the large salesman result

    Given the "large" traveling salesman result and distances to the home
    point, the function includes as many places to a trip limited by
    *max_time_per_trip* and returns a tuple with list of places represented by
    categories and actual time need for that trip.
    """
    categories_read = []
    salesman_sequence = []
    home_index_in_sequence = None
    with open(salesman_filename) as salesman_file:
        for line in salesman_file:
            # TODO: suboptimal check
            if line.startswith("sequence;"):
                continue
            unused_order, category, cost_to_next = line.split(";")
            try:
                category = int(category)
                cost_to_next = float(cost_to_next)
            except ValueError as err:
                gs.fatal(
                    _("Expected int;int;float in file 1 got: {line} ({error})").format(
                        line=line, error=err
                    )
                )
            if category in categories_read:
                # TODO: more proper error handling
                gs.warning(
                    _("Category appears twice in salesman output: ") + str(category)
                )
            categories_read.append(category)
            if category == home_category:
                # home will be appended next, so the current length
                # will become its index
                home_index_in_sequence = len(salesman_sequence)
            salesman_sequence.append((category, cost_to_next))

    if home_index_in_sequence is None:
        gs.fatal(
            _("Cannot find home category <{}> in salesman result").format(home_category)
        )

    cost_from_home = 0
    costs_from_home = []
    cost_from_last = 0
    for i in range(len(salesman_sequence)):
        index = home_index_in_sequence + i
        if index >= len(salesman_sequence):
            index -= len(salesman_sequence)
        category, cost_to_next = salesman_sequence[index]
        costs_from_home.append((category, cost_from_home, cost_from_last))
        cost_from_home += cost_to_next
        cost_from_last = cost_to_next
    # We could start in the direction of lower cost from home here
    # or we could do both directions and either compute both, possibly
    # in parallel, and get two trips (if they are not overlapping)
    # or pick the one which has lower time for min places
    # per trip or its time is closer to max time per trip.

    costs_to_home = {}
    # expecting: v.db.select to_home sep=";" -c > ...txt
    with open(to_home_filename) as to_home_file:
        for line in to_home_file:
            category, unused_value, distance = line.split(";")
            try:
                category = int(category)
                distance = float(distance)
            except ValueError as err:
                gs.fatal(
                    _("Expected int;int;float in file 2 got: {line} ({error})").format(
                        line=line, error=err
                    )
                )
            costs_to_home[category] = float(distance)

    trip_places = []
    total_trip_time = 0
    already_resolved_out_of_order = []
    last_time_to_home = 0
    for category, unused_from_home, from_last in costs_from_home:
        if category == home_category:
            continue
        if category in already_resolved_out_of_order:
            continue
        to_home = costs_to_home[category]
        time_to_home = dist_to_time * to_home
        time_from_last = dist_to_time * from_last
        number_of_places_in_step = 1
        time_in_current_place = time_per_place
        category_column = "cat"
        # TODO: make this a user input
        if multiplace_column:
            query_result = gs.vector_db_select(
                map=salesman,
                layer=2,
                where="{cat_col} = '{cat}'".format(
                    cat_col=category_column, cat=category
                ),
                columns=multiplace_column,
            )
            place_identifier = query_result["values"][category][0]
            if place_identifier:
                # TODO: the value is now assumed to be string
                query_result = gs.vector_db_select(
                    map=salesman,
                    layer=2,
                    where="{column} = '{value}'".format(
                        column=multiplace_column, value=place_identifier
                    ),
                    columns=category_column,
                )
                all_related_categories = query_result["values"].keys()
                number_of_places_in_step = len(all_related_categories)
                if number_of_places_in_step > 1:
                    print(
                        get_collapsing_message(place_identifier, all_related_categories)
                    )
                    time_in_current_place = number_of_places_in_step * time_per_place
                    # we store only in the case when we have some out of
                    # the original order of iteration (we also include
                    # the current one which we don't have to include)
                    already_resolved_out_of_order += all_related_categories
                    # TODO: do not end the search in this case and try
                    # to fit the next place
        step_time = time_from_last + time_in_current_place
        proposed_trip_time = total_trip_time + step_time + time_to_home
        if proposed_trip_time <= max_time_per_trip:
            if number_of_places_in_step == 1:
                trip_places.append(category)
            else:
                trip_places += all_related_categories
            total_trip_time += step_time
            last_time_to_home = time_to_home
        else:
            # add time of returning back home to the total trip time
            total_trip_time += last_time_to_home
            break
    return trip_places, total_trip_time


def main():
    """Runs traveling salesman and finds a smaller trip in it, then repeats"""
    options, unused_flags = gs.parser()

    network = options["input"]
    trip_basename = options["output_basename"]
    places_cats = options["to_cats"]
    home_cat = int(options["from_cat"])

    max_times = [float(time) for time in options["max_time"].split(",") if time]
    max_times_index = 0
    num_max_times = len(max_times)
    max_time_per_trip = max_times[max_times_index]

    time_per_place = float(options["place_time"])
    speed = float(options["speed"])
    dist_to_time = 1 / speed / 1000

    min_places = options["min_places"]
    if min_places:
        min_places = int(min_places)
        user_min_places = min_places
    else:
        # any number of places by default
        min_places = 0
        user_min_places = None

    points_with_attributes = options["attribute_map"]
    multiplace_column = options["multiplace_column"]
    name_column = options["name_column"]

    # TODO: Clean temporary files.
    # TODO: Create unique file names for the temporary files.
    salesman_filename = "salesman.txt"
    to_home_filename = "to_home_distances.txt"
    salesman_output = "tmp_salesman"
    to_home = "tmp_to_home"

    places_cats_unpacked = unpack_cats(map_name=network, layer=2, cats=places_cats)

    # TODO: check (and possibly sanitize) the inputs before this call
    # from_cat needs to exist
    # to_cats need to exist
    # from_cat should not be in to_cat
    # (or possibly from_cat in to_cat can be allowed but cleaned)
    # remove also cats which are not present in the map
    # (understanding the parameter to work as selection pattern, not an
    # exhaustive list)
    gs.run_command(
        "v.net.distance",
        input=network,
        output=to_home,
        from_layer=2,
        to_layer=2,
        from_cats=places_cats,
        to_cats=home_cat,
        quiet=True,
    )
    gs.run_command(
        "v.db.select",
        map=to_home,
        sep=";",
        flags="c",
        file=to_home_filename,
        quiet=True,
    )

    trips = []
    times = []
    remaining_places = set()
    never_resolved = None
    salesman_cats = ",".join([places_cats, str(home_cat)])

    # TODO: make it stop on number of points
    # (or more if there is a mechanism for postponing to a next trip)
    step_number = 0
    while True:
        step_number += 1
        salesman_output = "tmp_salesman_" + str(step_number)
        # v.net.salesman assigns same category to a point with the same
        # coordinates and the original category is lost
        gs.run_command(
            "v.net.salesman",
            input=network,
            center_cats=salesman_cats,
            arc_layer=1,
            node_layer=2,
            output=salesman_output,
            sequence=salesman_filename,
            quiet=True,
        )
        # TODO: check column names in inputs before this call
        if points_with_attributes and multiplace_column:
            columns = [multiplace_column]
            if name_column:
                columns.append(name_column)
            query_vector(
                map_name=salesman_output,
                layer=2,
                query_map=points_with_attributes,
                query_layer=1,
                columns=columns,
            )
        trip_places, trip_time = find_trip(
            salesman_output,
            salesman_filename,
            to_home_filename,
            home_cat,
            max_time_per_trip=max_time_per_trip,
            time_per_place=time_per_place,
            dist_to_time=dist_to_time,
            multiplace_column=multiplace_column,
        )

        next_max_trip_might_be_better = False
        not_enough_places_in_trip = False
        if max_times_index + 1 < num_max_times and (
            any_diff_smaller_than(
                value=trip_time,
                items=max_times[max_times_index + 1 :],
                difference=max_time_per_trip - trip_time,
            )
            or less_than_any(trip_time, max_times[max_times_index + 1 :])
        ):
            next_max_trip_might_be_better = True
        if len(trip_places) < min_places:
            not_enough_places_in_trip = True

        if (
            not trip_places
            or not_enough_places_in_trip
            or next_max_trip_might_be_better
        ):
            if not trip_places:
                gs.message(_("Cannot make a trip in given time"))
            elif not_enough_places_in_trip:
                gs.message(
                    _(
                        "Cannot fit enough places to one trip"
                        " (planned: {num},"
                        " requested: {min_places})"
                    ).format(num=len(trip_places), min_places=min_places)
                )
            if next_max_trip_might_be_better:
                old_max_time_per_trip = max_time_per_trip
                max_times_index += 1
                max_time_per_trip = max_times[max_times_index]
                if old_max_time_per_trip < max_time_per_trip:
                    gs.message(
                        _("Now planning longer trips: {new} (instead of {old})").format(
                            new=max_time_per_trip, old=old_max_time_per_trip
                        )
                    )
                else:
                    gs.message(
                        _(
                            "Now planning shorter trips: {new} (instead of {old})"
                        ).format(new=max_time_per_trip, old=old_max_time_per_trip)
                    )
                continue
            if trip_places and min_places > 1:
                old_min_places = min_places
                min_places -= 1
                # 0 is actually used too for any number of places
                gs.message(
                    _(
                        "Relaxing condition for minimum number"
                        " of places to visit"
                        " (requested: {requested}, now: {current})"
                    ).format(requested=old_min_places, current=min_places)
                )
                # TODO: we could also speed it up here for the last trip
                # (or last few trips) if we just to len(remaining_places)
                # instead of subtracting by one
                continue
            # Although the changes to parameters above help to
            # resolve many cases, when user requests multi places,
            # it is quite likely that this happens which makes
            # sense if keeping the places in a multi place together
            # is a hard requirement (if not we could temporarily
            # disable it).
            gs.warning(
                _("Cannot make more trips (time {} is too short)").format(
                    max_time_per_trip
                )
            )
            never_resolved = remaining_places
            break
        for cat in trip_places:
            if remaining_places and cat not in remaining_places:
                # TODO: we should copy here to be read only on network
                # (altenrnative is changing specificication of inputs,
                # i.e. attribute table would have to be part of the
                # network which would be ideal if there is an easy way
                # of achieving that)
                # add attributes to the original network
                columns = []
                if multiplace_column:
                    columns.append(multiplace_column)
                if name_column:
                    columns.append(name_column)
                if columns:
                    query_vector(
                        map_name=network,
                        layer=2,
                        query_map=points_with_attributes,
                        query_layer=1,
                        columns=columns,
                    )
                examine_place_and_fatal_error(
                    category=cat,
                    trip_places=trip_places,
                    remaining_places=remaining_places,
                    current_map=salesman_output,
                    current_layer=2,
                    master_map=network,
                    master_layer=2,
                    multiplace_column=multiplace_column,
                    name_column=name_column,
                )
        trips.append(trip_places)
        times.append((trip_time, max_time_per_trip))
        print("Trip places:", to_cats_parameter(trip_places))
        all_places = set(places_cats_unpacked)
        all_places = all_places.union({home_cat})
        remaining_places = all_places - set(flatten_list(trips))
        if not remaining_places - {home_cat}:
            break
        print("Remaining places:", to_cats_parameter(remaining_places))
        salesman_cats = to_cats_parameter(remaining_places)

    for i, trip in enumerate(trips):
        salesman_cats = to_cats_parameter(trip + [home_cat])
        # TODO: is there a Python function for basename in G API?
        salesman_output = trip_basename + "_" + str(i + 1)
        gs.run_command(
            "v.net.salesman",
            input=network,
            center_cats=salesman_cats,
            arc_layer=1,
            node_layer=2,
            output=salesman_output,
            quiet=True,
        )
        gs.vector_history(salesman_output)
        # TODO: it would be better if this is already in the attr table
        if points_with_attributes:
            query_vector(
                map_name=salesman_output,
                layer=2,
                query_map=points_with_attributes,
                query_layer=1,
            )

    if never_resolved:
        never_resolved_output = "never_resolved"
        gs.run_command(
            "v.extract",
            input=points_with_attributes,
            output=never_resolved_output,
            cats=to_cats_parameter(never_resolved),
            quiet=True,
        )
        gs.vector_history(never_resolved_output)
    report(
        trips=trips,
        times=times,
        never_resolved=never_resolved,
        user_min_places=user_min_places,
        max_time_per_trip=max_time_per_trip,
    )


if __name__ == "__main__":
    main()
