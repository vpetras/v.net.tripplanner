# Sampling trip planner

This is a GRASS GIS module *v.net.tripplanner* for planning sampling trips.

![changing paths for different sets of points](v_net_tripplanner.gif "Trips to cover all the places to visit")

See the [documentation for the module](https://vpetras.gitlab.io/v.net.tripplanner/).

## Install

```
g.extension v.net.tripplanner url=gitlab.com/vpetras/v.net.tripplanner
```

## Author

Vaclav Petras, [NCSU GeoForAll Lab](http://geospatial.ncsu.edu/geoforall/)

Copyright 2018 by the author

## License

GNU GPL v2 or later (https://spdx.org/licenses/GPL-2.0-or-later.html)
